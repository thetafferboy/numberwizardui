﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public void LoadLevel(string name) {
		Debug.Log ("Trying to load level: " + name);
		Application.LoadLevel(name);
	}
	
	public void QuitGame() {
		Debug.Log ("Quitting game...");
		Application.Quit ();
		}
}
