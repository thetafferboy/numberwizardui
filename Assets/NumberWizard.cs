﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NumberWizard : MonoBehaviour {
	int max;
	int min;
	int guess;
	int maxguess = 10;
	
	public Text guesstext;
	
		
	// Use this for initialization
	void Start () {
		StartGame();
	}
	
	
	// Update is called once per frame
	
	public void GuessHigher() {
		min = guess;
		NextGuess();
		}	
	
	public void GuessLower() {
		max = guess;
		NextGuess();
		}	
		
	
	void StartGame () {
		max = 1000;
		min = 1;
		guess = Random.Range (1,1001);
		guesstext.text = guess.ToString();
	}
	
	void NextGuess () {
		// guess = (max + min) / 2;
		guess = Random.Range(min, max+1);
		guesstext.text = guess.ToString();
		maxguess = maxguess - 1;
		
		if(maxguess <= 0) {
			Application.LoadLevel("Win");
			}
	}
}
